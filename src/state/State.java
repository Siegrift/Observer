package state;

/**
 * Created by siegrift on 5/3/17.
 */
public final class State {
  private static State state = null;
  private int rows;
  private int cols;
  private boolean rendering = false;

  public static State getState() {
    if (state == null) {
      state = new State();
    }
    return state;
  }

  public int getRows() {
    return rows;
  }

  public int getCols() {
    return cols;
  }

  public void setRendering(boolean rendering) {
    this.rendering = rendering;
  }

  public boolean isRendering() {
    return rendering;

  }
}
