package code;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;
import settings.Settings;
import settings.SettingsLoader;

public class Main extends Application {

  @Override
  public void start(Stage primaryStage) throws IOException {
    Settings settings = new Settings();
    SettingsLoader.loadSettings(settings, this.getParameters().getRaw());

    FXMLLoader loader;
    if (settings.inDevMode()) {
      loader = new FXMLLoader(getClass().getResource("developer.fxml"));
    } else {
      loader = new FXMLLoader(getClass().getResource("observer.fxml"));
    }
    Parent root = loader.load();
    Controller controller = loader.getController();
    controller.setSettings(settings);
    controller.initialize();

    int width = settings.getWindowWidth();
    int height = settings.getWindowHeight();
    Scene scene = new Scene(root, width, height);
    primaryStage.getIcons().add(new Image("res/icon.png"));
    primaryStage.setScene(scene);
    primaryStage.setOnCloseRequest(event -> System.exit(0));

    Path currentPath = Paths.get(System.getProperty("user.dir"));
    Path path = Paths.get(currentPath.toString(), "styles", "style.css");
    if (Files.exists(path)) {
      root.getStylesheets().add(path.toUri().toURL().toExternalForm());
    }
    if (settings.isWindowMaximized()) {
      primaryStage.setMaximized(true);
    }
    primaryStage.show();

    ObserverGraphics graphics = new ObserverGraphics(controller);
    ObserverLogic loop = new ObserverLogic(primaryStage, graphics, controller, settings);
    new Thread(loop).start();
  }

  public static void main(String[] args) {
    launch(args);
  }
}
