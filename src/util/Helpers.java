package util;

import settings.Settings;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import state.State;

/**
 * Created by siegrift on 4/23/17.
 */
public class Helpers {
  public static String colorToHex(Color c, double alpha) {
    int r = (int) (c.getRed() * 255);
    int g = (int) (c.getGreen() * 255);
    int b = (int) (c.getBlue() * 255);
    int a = (int) (alpha * 255);
    return String.format("#%02x%02x%02x%02x", r, g, b, a);
  }

  public static void calculateCellSize(Pane pane, Settings settings) {
    int xs = (int) (pane.getWidth() / State.getState().getCols());
    int ys = (int) (pane.getWidth() / State.getState().getCols());
    settings.setSquareSize(String.valueOf(Math.min(xs, ys)));
  }
}
