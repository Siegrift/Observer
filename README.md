## Observer

Observer je graficky program, v ktorom vidite co vasi boti robia.

Na rozbehanie observera treba pouzivat Oracle javu, mozno staci 
`sudo apt-get install oracle-java8-installer`
Tento link vsak aspon mne nefuguje.

Treba postupovat podla tohto linku

http://www.webupd8.org/2012/09/install-oracle-java-8-in-ubuntu-via-ppa.html

Ak toto nebude stacit treba skusit instrukcie z tohto linku.

'http://askubuntu.com/questions/430434/replace-openjdk-with-oracle-jdk-on-ubuntu'

### Pouzivanie observera

Observer najdete na stranke proboja, ktoru zverejni veduci, malo by tam byt
vsetko co potrebujete. Ak tam nieco chyba, kopte do veduceho.

Observer standardne spustame cez terminal. Ocakava 1 nutny parameter a ten
je cesta k priecinku, kde sa nachadza subor 'observation'.

Observer vie dostavat aj nepovinne argumenty, ktore vacsinou zacinaju prefixom
'--' (napr. --size=50). Vacsina z tychto nepovinnych argumentov su zbytocne,
a nebudete ich potrebovat.

TODO dopisat zvysok